﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [WeatherForecast] (
    [Id] int NOT NULL IDENTITY,
    [Date] datetime2 NOT NULL,
    [TemperatureC] int NOT NULL,
    [Summary] nvarchar(max) NULL,
    CONSTRAINT [PK_WeatherForecast] PRIMARY KEY ([Id])
);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200630190757_InitialCreate', N'3.1.5');

GO

ALTER TABLE [WeatherForecast] ADD [Country] nvarchar(max) NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200701215032_add column country', N'3.1.5');

GO

