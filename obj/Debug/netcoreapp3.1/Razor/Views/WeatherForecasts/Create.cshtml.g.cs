#pragma checksum "C:\Users\mtzfabiosilva\source\repos\BlazorApp1\BlazorApp1\Views\WeatherForecasts\Create.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "354b7dbc3d9ae72060e5917584a5e2d3c3905e66"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_WeatherForecasts_Create), @"mvc.1.0.view", @"/Views/WeatherForecasts/Create.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"354b7dbc3d9ae72060e5917584a5e2d3c3905e66", @"/Views/WeatherForecasts/Create.cshtml")]
    public class Views_WeatherForecasts_Create : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<BlazorApp1.Data.WeatherForecast>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\mtzfabiosilva\source\repos\BlazorApp1\BlazorApp1\Views\WeatherForecasts\Create.cshtml"
  
    ViewData["Title"] = "Create";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<h1>Create</h1>

<h4>WeatherForecast</h4>
<hr />
<div class=""row"">
    <div class=""col-md-4"">
        <form asp-action=""Create"">
            <div asp-validation-summary=""ModelOnly"" class=""text-danger""></div>
            <div class=""form-group"">
                <label asp-for=""Date"" class=""control-label""></label>
                <input asp-for=""Date"" class=""form-control"" />
                <span asp-validation-for=""Date"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""TemperatureC"" class=""control-label""></label>
                <input asp-for=""TemperatureC"" class=""form-control"" />
                <span asp-validation-for=""TemperatureC"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""Summary"" class=""control-label""></label>
                <input asp-for=""Summary"" class=""form-control"" />
                <span asp-validation-for=""Summary"" class=""text-danger");
            WriteLiteral(@"""></span>
            </div>
            <div class=""form-group"">
                <input type=""submit"" value=""Create"" class=""btn btn-primary"" />
            </div>
        </form>
    </div>
</div>

<div>
    <a asp-action=""Index"">Back to List</a>
</div>

");
            DefineSection("Scripts", async() => {
                WriteLiteral("\r\n");
#nullable restore
#line 42 "C:\Users\mtzfabiosilva\source\repos\BlazorApp1\BlazorApp1\Views\WeatherForecasts\Create.cshtml"
      await Html.RenderPartialAsync("_ValidationScriptsPartial");

#line default
#line hidden
#nullable disable
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<BlazorApp1.Data.WeatherForecast> Html { get; private set; }
    }
}
#pragma warning restore 1591
